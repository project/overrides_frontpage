<?php

namespace Drupal\overrides_frontpage\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom Frontpage for Authenticated users settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create function for depdendency injection.
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'overrides_frontpage';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['overrides_frontpage.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $saved_field__loggedin = $this->config('overrides_frontpage.settings')->get('overrides_frontpage.field_loggedin_frontpage');
    $form['field_loggedin_frontpage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authenticated User frontpage'),
      '#default_value' => $saved_field__loggedin ? "/webservices" : NULL,
    ];
    $saved_field__loggedout = $this->config('overrides_frontpage.settings')->get('overrides_frontpage.field_loggedout_frontpage');
    $form['field_loggedout_frontpage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anonymus User frontpage'),
      '#default_value' => $saved_field__loggedout ? "/user/register" : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('overrides_frontpage.settings')
      ->set('overrides_frontpage.field_loggedin_frontpage', $form_state->getValue('field_loggedin_frontpage'))
      ->save();
    $this->config('overrides_frontpage.settings')
      ->set('overrides_frontpage.field_loggedout_frontpage', $form_state->getValue('field_loggedout_frontpage'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
