<?php

namespace Drupal\overrides_frontpage\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * ConfigFactory override for authenticated frontpage.
 */
class AuthenticatedFrontpageOverrides implements ConfigFactoryOverrideInterface {

  /**
   * Overrides system.site configuration when a user is authenticated.
   *
   * @param array $names
   *   Configuration names to load.
   *
   * @return array
   *   An array of overrides.
   */
  public function loadOverrides($names) {
    $overrides = [];
    if (in_array('system.site', $names)) {

      // Get base URL of the site.
      $site_url = $this->getUrlSite();

      // Get loggedin_frontpage config value.
      $configFactory = \Drupal::configFactory();
      $loggedin_frontpage = $configFactory->get('overrides_frontpage.settings')
        ->get('overrides_frontpage.field_loggedin_frontpage');

      // If a user is authenticated, change front page to the loggedin_frontpage.
      if (!empty($loggedin_frontpage) && \Drupal::service('current_user')->isAuthenticated()) {
        $loggedin_frontpage = $site_url . $loggedin_frontpage;
        $overrides['system.site']['page'] = ['front' => $loggedin_frontpage];
      }

      // Get loggedout_frontpage config value.
      $configFactory = \Drupal::configFactory();
      $loggedout_frontpage = $configFactory->get('overrides_frontpage.settings')
        ->get('overrides_frontpage.field_loggedout_frontpage');
      // If a user is anonymus, change front page to the loggedin_frontpage.
      if (\Drupal::currentUser()->isAnonymous()) {
        $overrides['system.site']['page'] = ['front' => $loggedout_frontpage];
      }
    }
    return $overrides;
  }

  /**
   * Retrieves the base URL of the site.
   *
   * @return string
   *   The base URL.
   */
  private function getUrlSite() {
    $request = \Drupal::request();
    $site_url = $request->getSchemeAndHttpHost();
    return $site_url;
  }

  /**
   * Returns the string to append to all configuration object names.
   *
   * @return string
   *   A string to append to all configuration object names.
   */
  public function getCacheSuffix() {
    return 'AuthenticatedFrontpageOverrider';
  }

  /**
   * Returns a ConfigCacheableMetadata instance to attach to configuration objects.
   *
   * @param string $name
   *   The configuration object name.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   A cacheable metadata object.
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * Returns an empty ConfigObject.
   *
   * @param string $name
   *   The configuration object name.
   * @param string $collection
   *   The collection to which this configuration object belongs.
   *
   * @return null
   *   An empty ConfigObject.
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }
}
